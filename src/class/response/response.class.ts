import { IBaseResponse } from "src/interface/response/response.interface";

export default class CustomizeResponse implements IBaseResponse {
    statusCode: string | number;
    response: Record<string, any>;

    constructor(statusCode: string | number, data: Record<string, any>) {
        this.statusCode = statusCode;
        this.response = data;
    }

    public getBaseResponse(): IBaseResponse {
        return {
            statusCode: this.statusCode,
            response: this.response,
        };
    }
}
