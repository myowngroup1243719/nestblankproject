import { ApiProperty } from "@nestjs/swagger";

export class DoroDto implements IDoroDtoInterface {
    @ApiProperty()
    word: string;
}

export interface IDoroDtoInterface {
    word: string;
}
