import { Module } from "@nestjs/common";
import DororongController from "src/controller/dororong.controller";
import DororongService from "./dororong.service";

@Module({
    imports: [],
    controllers: [DororongController],
    exports: [DororongService],
    providers: [DororongService],
})
export default class DororongModule {}
