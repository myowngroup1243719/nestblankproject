import { Injectable } from "@nestjs/common";

@Injectable()
export default class DororongService {
    constructor() {}

    public async helloDoro(word: string) {
        return `Hello Mister ${word}`;
    }
}
