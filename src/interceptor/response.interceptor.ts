import {
    CallHandler,
    ExecutionContext,
    HttpException,
    HttpStatus,
    Injectable,
    NestInterceptor,
} from "@nestjs/common";
import { Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";
import CustomizeResponse from "src/class/response/response.class";

@Injectable()
export default class ResponseInterceptor implements NestInterceptor {
    intercept(
        context: ExecutionContext,
        next: CallHandler<any>
    ): Observable<any> | Promise<Observable<any>> {
        const req = context.switchToHttp().getRequest();
        console.log(
            `Request Body ===> `,
            req.body,
            ` at ${new Date().toISOString()}`
        );

        return next
            .handle()
            .pipe(
                map((res) => {
                    const response = new CustomizeResponse(200, res);
                    return response.getBaseResponse();
                })
            )
            .pipe(
                catchError((err) => {
                    const status = err.response.status;

                    if (status >= HttpStatus.BAD_REQUEST) {
                        throw new HttpException(err.response.data, status);
                    }

                    throw new HttpException(
                        `Error Naja Doro`,
                        HttpStatus.INTERNAL_SERVER_ERROR
                    );
                })
            );
    }
}
