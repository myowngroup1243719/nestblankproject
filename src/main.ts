import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

/**
 * This is a function to Bootstrap the server;
 *
 */
async function bootstrap(): Promise<void> {
    const app = await NestFactory.create(AppModule);

    const env = "DEV";
    if (env == "DEV") {
        const config = new DocumentBuilder()
            .setTitle("My Little Doro.")
            .setDescription("The Doro API description")
            .setVersion("1.0")
            .addTag("Doro")
            .build();

        const document = SwaggerModule.createDocument(app, config);
        SwaggerModule.setup(`doro-api`, app, document);
    }

    app.enableCors({
        origin: ["https://myWebSite.com", "http://localhost:3000"],
        methods: ["GET", "POST", "PUT", "DELETE"],
        allowedHeaders: ["Content-Type", "Authorization"],
        credentials: true, // Set to true if you need to allow credentials (cookies, authorization headers)
    });

    app.listen(3969).finally(() => {
        console.log(`The Server is running on port 3969`);
    });
}

bootstrap();
