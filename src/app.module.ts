import { Module, Provider } from "@nestjs/common";
import { APP_INTERCEPTOR } from "@nestjs/core";
import ResponseInterceptor from "./interceptor/response.interceptor";
import DororongModule from "./class/dororong/dororong.module";

const providers: Provider[] = [
    {
        provide: APP_INTERCEPTOR,
        useClass: ResponseInterceptor,
    },
];

@Module({
    imports: [DororongModule],
    controllers: [],
    providers: providers,
    exports: [],
})
export class AppModule {}
