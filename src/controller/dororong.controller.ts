import { Body, Controller, Post } from "@nestjs/common";
import { DoroDto } from "src/class/dororong/dororong.dto";
import DororongService from "src/class/dororong/dororong.service";

@Controller(`doro`)
export default class DororongController {
    constructor(private readonly _doroService: DororongService) {}

    @Post("hello")
    public async helloDoro(@Body() req: DoroDto) {
        return this._doroService.helloDoro(req.word);
    }
}
