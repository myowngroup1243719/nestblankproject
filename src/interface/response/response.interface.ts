export interface IBaseResponse {
    statusCode: string | number;
    response: Record<string, any>;
}

export interface IBaseResponseMessage {
    message: string;
}

export interface IResponseHealthCheck {
    data: boolean;
}
